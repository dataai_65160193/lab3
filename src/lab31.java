public class lab31 {
    public static int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }
        
        int left = 1;
        int right = x;
        int result = 0;
        
        while (left <= right) {
            int mid = left + (right - left) / 2;
            
            // Use long to prevent integer overflow
            long square = (long) mid * mid;
            
            if (square == x) {
                return mid;
            } else if (square < x) {
                left = mid + 1;
                result = mid; // Store the result when square < x
            } else {
                right = mid - 1;
            }
        }
        
        return result;
    }

    public static void main(String[] args) {
        int x1 = 4;
        System.out.println("Output for Example 1: " + mySqrt(x1));

        int x2 = 8;
        System.out.println("Output for Example 2: " + mySqrt(x2));
    }
}
